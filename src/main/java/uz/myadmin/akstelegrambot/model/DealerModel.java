package uz.myadmin.akstelegrambot.model;

import lombok.Data;

@Data
public class DealerModel {
    private Long id;
    private String name;
}
