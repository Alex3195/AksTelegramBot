package uz.myadmin.akstelegrambot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.myadmin.akstelegrambot.entity.ActionHistory;

public interface ActionHistoryRepository extends JpaRepository<ActionHistory, Long> {
    ActionHistory findActionHistoryByChatId(Long chatId);
}
