package uz.myadmin.akstelegrambot.handler.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.myadmin.akstelegrambot.commands.Commands;
import uz.myadmin.akstelegrambot.constant.Status;
import uz.myadmin.akstelegrambot.handler.Handler;
import uz.myadmin.akstelegrambot.model.BotUserModel;
import uz.myadmin.akstelegrambot.service.TelegramService;
import uz.myadmin.akstelegrambot.service.UsersService;

@Service
@RequiredArgsConstructor
public class ContactHandler implements Handler<Message> {
    private final TelegramService telegramService;
    private final UsersService usersService;

    @Override
    public void handleMessage(Message message) throws TelegramApiException {
        BotUserModel model = usersService.saveUser(message);
        if (model.getStatus() == Status.PASSIVE) {
            telegramService.executeMessage(SendMessage
                    .builder()
                    .chatId(message.getChatId().toString())
                    .text("Ваш аккаунт неактивен, обратитесь к администратору.")
                    .replyToMessageId(message.getMessageId())
                    .replyMarkup(Commands.getCheckActiveUser())
                    .build());
        }else {
            telegramService.executeMessage(SendMessage
                    .builder()
                    .chatId(message.getChatId().toString())
                    .text("Ваш аккаунт активен.")
                    .replyToMessageId(message.getMessageId())
                    .replyMarkup(Commands.getSubMenuReplyKeyboard())
                    .build());
        }
    }
}

