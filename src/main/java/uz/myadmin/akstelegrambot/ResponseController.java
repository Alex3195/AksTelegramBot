package uz.myadmin.akstelegrambot;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.myadmin.akstelegrambot.service.TelegramScheduleService;

import java.util.Map;

@RequiredArgsConstructor
@RestController
@RequestMapping("/bot/api")
public class ResponseController {
    private final TelegramScheduleService telegramScheduleService;

    @PostMapping("/sendProductPriceToUsers")
    public String sendChangedPriceToUsers(@RequestBody Map map) throws TelegramApiException {
        telegramScheduleService.sendChangedPriceToUsers(map);
        return "task is completed";
    }
}
