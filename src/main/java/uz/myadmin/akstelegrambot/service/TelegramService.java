package uz.myadmin.akstelegrambot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;


@Service
public class TelegramService extends DefaultAbsSender {
    @Value("${telegram.bot.token}")
    private String token;

    protected TelegramService() {
        super(new DefaultBotOptions());
    }

    public void executeMessage(SendMessage message){
       try {
           execute(message);
       }catch (TelegramApiException e){
           e.printStackTrace();
       }
    }

    public void executeDocument(SendDocument document) throws TelegramApiException {
        execute(document);
    }

    public void executePhoto(SendPhoto photo) throws TelegramApiException {
        execute(photo);
    }

    @Override
    public String getBotToken() {
        return token;
    }
}
