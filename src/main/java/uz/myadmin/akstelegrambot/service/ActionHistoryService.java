package uz.myadmin.akstelegrambot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.myadmin.akstelegrambot.commands.Commands;
import uz.myadmin.akstelegrambot.entity.ActionHistory;
import uz.myadmin.akstelegrambot.repository.ActionHistoryRepository;

@Service
@RequiredArgsConstructor
public class ActionHistoryService {
    private final ActionHistoryRepository repository;

    public void saveOrUpdate(String action, Long chatId) {
        ActionHistory actionHistory = repository.findActionHistoryByChatId(chatId);
        if (actionHistory != null) {
            String actions = actionHistory.getAction();

            if (!actionHistory.getAction().equalsIgnoreCase(action) && !action.equalsIgnoreCase("Все вместе") && !action.equalsIgnoreCase("Список дилеров")) {
                if (actions.split("/").length > 1) {
                    String innerStringAction = actions.split("/")[actions.split("/").length - 1];
                    switch (innerStringAction) {
                        case Commands.SYNCHRONIZE:
                            if (action.equalsIgnoreCase(Commands.GET_ALL_SYNC)) {
                                actions = actions.concat("/").concat(action);
                            } else {
                                actions = actions.replace(innerStringAction, action);
                            }
                            break;
                        case Commands.REMAINING_GOODS:
                            actions = actions.replace(innerStringAction, action);
                            break;
                        case Commands.GET_ACTIONS_OF_TODAY:
                            actions = actions.replace(innerStringAction, action);
                            break;
                        default:
                            actions = actions.concat("/").concat(action);
                            break;
                    }
                } else {
                    actions = actions.concat("/").concat(action);
                }
            } else {
                actions = action;

            }
            actionHistory.setAction(actions);
        } else {
            actionHistory = new ActionHistory();
            actionHistory.setAction(action);
        }
        actionHistory.setChatId(chatId);
        repository.save(actionHistory);

    }

    public String getLastActionByChatId(Long chatId) {
        ActionHistory history = repository.findActionHistoryByChatId(chatId);
        String actions = history.getAction();
        if (actions.endsWith("/")) {
            actions = actions.substring(0, actions.length() - 1);
        }
        String lastAction = actions.split("/")[actions.split("/").length - 1];
        String res = actions.split("/")[actions.split("/").length > 2 ? actions.split("/").length - 2 : 0];
        int lenth = actions.length() - lastAction.length() - 1;
        if (lenth == -1) {
            history.setAction("");
            history = repository.save(history);
        } else {
            history.setAction(actions.substring(0, lenth));
            history = repository.save(history);
        }
        if (lastAction.equalsIgnoreCase(Commands.LIST_DEALERS)) {
            return lastAction;
        }

        if(history.getAction().isEmpty()){
            return history.getAction();
        }
        return res ;
    }

    public void backToFirstCommand(Long chatId) {
        ActionHistory history = repository.findActionHistoryByChatId(chatId);
        String action = history.getAction().split("/")[0];
        history.setAction(action);
        repository.save(history);
    }
}
