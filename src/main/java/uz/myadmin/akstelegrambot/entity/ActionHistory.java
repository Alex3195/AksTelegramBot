package uz.myadmin.akstelegrambot.entity;

import lombok.Data;

import javax.persistence.*;
@Data
@Entity
@Table(name = "action_history_telegram",schema = "prodaja_web")
public class ActionHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "user_chat_id",unique = true)
    private Long chatId;

    @Column(name = "action",columnDefinition = "TEXT")
    private String action;

}
