package uz.myadmin.akstelegrambot.service;

import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.myadmin.akstelegrambot.commands.Commands;
import uz.myadmin.akstelegrambot.constant.Status;
import uz.myadmin.akstelegrambot.model.BotUserModel;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@EnableScheduling
public class TelegramScheduleService {
    private final TelegramService telegramService;
    private final UsersService usersService;

    @Scheduled(fixedDelay = 60 * 60 * 1000)
    public void sendChangedPriceCount() throws TelegramApiException {
        List<BotUserModel> userList = usersService.getAll();

        for (BotUserModel model : userList) {
            if (model.getStatus() == Status.PASSIVE) {
                continue;
            }
            Message message = new Message();
            Chat chat = new Chat();
            chat.setId(model.getChatId());
            message.setChat(chat);
            JSONObject numberOfReports = usersService.getNumberOfReport(message, "");
            String msg = "------  Синхронизировать  ------\n\n";
            for (String key : numberOfReports.keySet()) {
                if (!numberOfReports.get(key).equals(0)) {
                    msg = msg + "Название дилера: " + key + "\n"
                            + "У вас " + numberOfReports.get(key) + " несинхронизированных продукта\n\n";
                    telegramService.executeMessage(
                            SendMessage.builder()
                                    .text(msg)
                                    .chatId(message.getChatId().toString())
                                    .replyMarkup(Commands.getSubMenuReplyKeyboard())
                                    .build()
                    );
                }

            }


        }
    }

//    @Scheduled(fixedDelay = 60 * 60 * 1000)
//    public void sendReportToUserEverySomeMinutes() throws TelegramApiException {
//        List<BotUserModel> userList = usersSevice.getAll();
//
//        for (BotUserModel model : userList) {
//            if (model.getStatus() == Status.PASSIVE) {
//                continue;
//            }
//            Message message = new Message();
//            Chat chat = new Chat();
//            chat.setId(model.getChatId());
//            message.setChat(chat);
//            JSONObject numberOfReports = usersSevice.getNumberOfReport(message, 0L);
//            String msg = "------  Товары на которые изменилась цена  ------\n\n";
//            for (String key : numberOfReports.keySet()) {
//                if (!numberOfReports.get(key).equals(0)) {
//                    msg = msg.concat("Название дилера: " + key + "\n");
//                    List<TelegramSyncReport> reportList = usersSevice.getAllReportByDealerId(message, 0L);
//                    for (TelegramSyncReport report : reportList) {
//                        String messageAllSyncText = "   Название продукта: " + report.getProductName() + "\n"
//                                + "   Дата: " + report.getOldDate() + "\n"
//                                + "   Старая цена: " + report.getOldPrice() + "\n"
//                                + "   Дата: " + report.getLastDate() + "\n"
//                                + "   Последняя цена: " + report.getLastPrice();
//                        msg = msg + messageAllSyncText + "\n\n";
//                    }
//                    msg = msg.concat("________________________________________\n");
//                    telegramService.executeMessage(
//                            SendMessage.builder()
//                                    .text(msg)
//                                    .chatId(message.getChatId().toString())
//                                    .replyMarkup(Commands.getSubMenuReplyKeyboard())
//                                    .build()
//                    );
//                }
//
//
//            }
//
//        }
//    }

    public void sendChangedPriceToUsers(Map map) {
        List<BotUserModel> list = usersService.getAll();
        String msg = "ProductName: " + map.get("name").toString() + "\n"
                + "Price: " + map.get("price").toString();
        for (BotUserModel model : list) {

            Message message = new Message();
            Chat chat = new Chat();
            chat.setId(model.getChatId());
            message.setChat(chat);
            telegramService.executeMessage(SendMessage
                    .builder()
                    .chatId(message.getChatId().toString())
                    .text(msg)
                    .replyMarkup(Commands.getSubMenuReplyKeyboard())
                    .build());
        }
    }
}
