package uz.myadmin.akstelegrambot.commands;

import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import uz.myadmin.akstelegrambot.constant.Status;
import uz.myadmin.akstelegrambot.model.BotUserModel;
import uz.myadmin.akstelegrambot.model.TelegramSyncReport;
import uz.myadmin.akstelegrambot.service.ActionHistoryService;
import uz.myadmin.akstelegrambot.service.TelegramService;
import uz.myadmin.akstelegrambot.service.UsersService;

import java.util.*;

@Service
@RequiredArgsConstructor
public class GeneralCommandHandler implements CommandHandler<Message> {
    private final TelegramService telegramService;
    private final UsersService usersService;
    private final ActionHistoryService actionHistoryService;
    private String name;
    private ReplyKeyboardMarkup replyKeyboardMarkup;

    @Override
    public void executeCommand(Message message, String command) {
        int commandLength = command.split("/").length;
        if (!command.equalsIgnoreCase(Commands.START)) {
            if (commandLength > 1) {
                name = command.split("/")[1].trim();
                command = command.split("/")[0].trim();
            } else {
                command = command.split("/")[0].trim();
                name = "";
            }
        }
        BotUserModel userModel = usersService.getUserByChtId(message);
        if (userModel != null) {
            switch (command) {
                case Commands.START:
                    telegramService.executeMessage(
                            SendMessage.builder()
                                    .text("hi " + message.getFrom().getFirstName())
                                    .chatId(message.getChatId().toString())
                                    .replyMarkup(Commands.getShareContactKeyBoard())
                                    .build()
                    );
                    break;
                case Commands.MAIN_MENU:
                    telegramService.executeMessage(
                            SendMessage.builder()
                                    .text("Main menu")
                                    .chatId(message.getChatId().toString())
                                    .replyMarkup(Commands.getSubMenuReplyKeyboard())
                                    .build()
                    );
                    break;
                case Commands.CHECK_STATUS_USER:
                    BotUserModel model = usersService.getUserStatus(message);
                    if (model.getStatus() == Status.PASSIVE) {
                        telegramService.executeMessage(SendMessage
                                .builder()
                                .chatId(message.getChatId().toString())
                                .text("Ваш аккаунт неактивен, обратитесь к администратору.")
                                .replyToMessageId(message.getMessageId())
                                .replyMarkup(Commands.getCheckActiveUser())
                                .build());
                    } else {
                        replyKeyboardMarkup = Commands.getSubMenuReplyKeyboard();
                        telegramService.executeMessage(SendMessage
                                .builder()
                                .chatId(message.getChatId().toString())
                                .text("Ваш аккаунт активен.")
                                .replyToMessageId(message.getMessageId())
                                .replyMarkup(replyKeyboardMarkup)
                                .build());
                    }
                    break;
                case Commands.BACK:
                    String action = actionHistoryService.getLastActionByChatId(message.getChatId());
                    switch (action) {
//                    case Commands.REMAINING_GOODS:
//                        replyKeyboardMarkup = Commands.getSubMenuReplyKeyboard();
//                        dealerId = 0L;
//                        actionHistoryService.backToFirstCommand(message.getChatId());
//                        break;
//                    case Commands.ALL_TOGETHER:
//                        replyKeyboardMarkup = Commands.getSubMenuReplyKeyboard();
//                        dealerId = 0L;
//                        actionHistoryService.backToFirstCommand(message.getChatId());
//                        break;
//                    case Commands.GET_ALL_SYNC:
//                        replyKeyboardMarkup = Commands.getSubMenuReplyKeyboard();
//                        dealerId = 0L;
//                        actionHistoryService.backToFirstCommand(message.getChatId());
//                        break;

                        case Commands.SYNCHRONIZE:
                            replyKeyboardMarkup = Commands.getMenuKeyboard(name);
                            break;
                        case Commands.LIST_DEALERS:
                            BotUserModel user = usersService.getUserByChtId(message);
                            if (replyKeyboardMarkup.equals(Commands.getMenuKeyboard(name))) {

                                replyKeyboardMarkup = Commands.getListOfDealersMarkup(user.getDealerModels());
                            } else {
                                if (replyKeyboardMarkup.equals(Commands.getListOfDealersMarkup(user.getDealerModels()))) {
                                    replyKeyboardMarkup = Commands.getListOfDealersMarkup(user.getDealerModels());
                                } else {
                                    replyKeyboardMarkup = Commands.getMenuKeyboard(name);
                                }
                            }
                            break;
                        case Commands.ALL_TOGETHER:
                            replyKeyboardMarkup = Commands.getMenuKeyboard(name);
                            break;
                        default:
                            actionHistoryService.backToFirstCommand(message.getChatId());
                            replyKeyboardMarkup = Commands.getSubMenuReplyKeyboard();
                            name = "";
                            break;
                    }
                    telegramService.executeMessage(
                            SendMessage.builder()
                                    .text("/")
                                    .chatId(message.getChatId().toString())
                                    .replyMarkup(replyKeyboardMarkup)
                                    .build()
                    );
                    break;
                case Commands.LIST_DEALERS:
                    actionHistoryService.saveOrUpdate(Commands.LIST_DEALERS, message.getChatId());
                    BotUserModel user = usersService.getUserByChtId(message);
                    replyKeyboardMarkup = Commands.getListOfDealersMarkup(user.getDealerModels());
                    telegramService.executeMessage(
                            SendMessage.builder()
                                    .text("dealer list")
                                    .chatId(message.getChatId().toString())
                                    .replyMarkup(replyKeyboardMarkup)
                                    .build()
                    );

                    break;
                case Commands.ALL_TOGETHER:
                    actionHistoryService.saveOrUpdate(Commands.ALL_TOGETHER, message.getChatId());
                    replyKeyboardMarkup = Commands.getMenuKeyboard(name);
                    telegramService.executeMessage(
                            SendMessage.builder()
                                    .text(Commands.MENU)
                                    .replyMarkup(replyKeyboardMarkup)
                                    .chatId(message.getChatId().toString())
                                    .build()
                    );
                    break;
                case Commands.GET_ACTIONS_OF_TODAY:
                    actionHistoryService.saveOrUpdate(Commands.GET_ACTIONS_OF_TODAY, message.getChatId());
                    Map dataActions = usersService.getActionsOfToday(message, name);
                    List<Map> data = (List) dataActions.get("data");
                    for (Map map1 : data) {
                        Set<String> set = map1.keySet();
                        for (String name : set) {
                            List<Map> mapList = (List<Map>) map1.get(name);
                            for (Map map : mapList) {
                                String messageTextActionOfToday = "------  Сегодняшний операции  ------\n" +
                                        name + "\n\n"
                                        + "Date: " + dataActions.get("date").toString() + "\n"
                                        + "Количество продуктов: " + map.get("product_qty").toString() + "\n\n"
                                        + "Начальный остаток: \n"
                                        + " \t Количество: " + map.get("from_ostatka_qty").toString() + "\n"
                                        + " \t Сумма: " + map.get("from_ostatka_summa").toString() + "\n\n"
                                        + "Конечный остаток: \n"
                                        + " \t Количество: " + map.get("to_ostatka_qty").toString() + "\n"
                                        + " \t Сумма: " + map.get("to_ostatka_summa").toString() + "\n\n"
                                        + "Прыход: \n"
                                        + "\t \t Количество: " + map.get("prixod_qty").toString() + "\n"
                                        + "\t \t Сумма: " + map.get("prixod_summa").toString() + "\n\n"
                                        + "Расходы: \n"
                                        + "\t Количество: " + map.get("rasxod_qty").toString() + "\n"
                                        + "\t Сумма: " + map.get("rasxod_summa").toString() + "\n\n"
                                        + "Возврадь : \n " +
                                        "\t Возврадь база : \n " +
                                        "\t \t \t Количество : " + map.get("vozvrat_baza_qty").toString() + "\n"
                                        + " \t \t \t Сумма :" + map.get("vozvrat_baza_summa").toString() + "\n"
                                        + "\t Возврадь клиент: \n " +
                                        "\t \t \t Количество : " + map.get("vozvrat_client_qty").toString() + "\n"
                                        + " \t \t \t Сумма :" + map.get("vozvrat_client_summa").toString() + "\n\n"
                                        + " Перемещения товаров :\n " +
                                        "\t Количество :" + map.get("godown_qty").toString() + " \n"
                                        + " \t Сумма: " + map.get("godown_summa").toString() + "\n\n"
                                        + "Бракованные товары: \n"
                                        + "\t Количество: " + map.get("brak_qty").toString() + "\n"
                                        + "\t Сумма: " + map.get("brak_summa").toString();

                                telegramService.executeMessage(
                                        SendMessage.builder()
                                                .text(messageTextActionOfToday)
                                                .chatId(message.getChatId().toString())
                                                .build()
                                );
                            }
                        }
                    }
                    break;

                case Commands.REMAINING_GOODS:
                    actionHistoryService.saveOrUpdate(Commands.REMAINING_GOODS, message.getChatId());
                    userModel = usersService.getUserByChtId(message);
                    if (userModel.getType().equalsIgnoreCase("OFFICE_MANAGER")) {
                        Map dataMap = usersService.getReportRemainingGoodsAllTogether(message, name);
                        String messageTextRemainingGoods = "------  Остаток товари  ------\n";
                        for (Object key : dataMap.keySet()) {
                            Map innerData = (LinkedHashMap) dataMap.get(key);
                            messageTextRemainingGoods = messageTextRemainingGoods.concat(
                                    "Дата: " + innerData.get("date").toString() + " \n"
                                            + "Дилер: " + innerData.get("dealerName").toString() + " \n"
                                            + "Кол-во видов товаров: " + innerData.get("countProductTypes").toString() + "\n"
                                            + "Кол-во ост товаров: " + innerData.get("countProducts") + "\n"
                                            + "Сумма стоимости товара: " + innerData.get("sumProductPrice") + "\n\n"
                            );
                        }
                        telegramService.executeMessage(
                                SendMessage.builder()
                                        .text(messageTextRemainingGoods)
                                        .chatId(message.getChatId().toString())
                                        .build());
                    } else {
                        Map dataMap = usersService.getReportRemainingGoods(message, name);

                        String messageTextRemainingGoods = "------  Остаток товари  ------\n";
                        for (Object key : dataMap.keySet()) {
                            Map innerData = (LinkedHashMap) dataMap.get(key);
                            messageTextRemainingGoods = messageTextRemainingGoods.concat(
                                    "Дата: " + innerData.get("date").toString() + " \n"
                                            + "Дилер: " + innerData.get("dealerName").toString() + " \n"
                                            + "Кол-во видов товаров: " + innerData.get("countProductTypes").toString() + "\n"
                                            + "Кол-во ост товаров: " + innerData.get("countProducts") + "\n"
                                            + "Сумма стоимости товара: " + innerData.get("sumProductPrice") + "\n\n"
                            );
                        }
                        telegramService.executeMessage(
                                SendMessage.builder()
                                        .text(messageTextRemainingGoods)
                                        .chatId(message.getChatId().toString())
                                        .build());
                    }

                    break;
                case Commands.GET_ALL_SYNC:
                    actionHistoryService.saveOrUpdate(Commands.GET_ALL_SYNC, message.getChatId());
                    replyKeyboardMarkup = Commands.getMenuKeyboard(name);
                    List<TelegramSyncReport> reportList = usersService.getAllReportByDealerId(message, name);

                    for (TelegramSyncReport report : reportList) {

                        String msg = "------  Товары на которые изменилась цена  ------\n\n";
                        String messageAllSyncText = "Название продукта: " + report.getProductName() + "\n"
                                + "Время записи данных в базу данных: " + report.getDate() + "\n"
                                + "Дата: " + report.getOldDate() + "\n"
                                + "Старая цена: " + report.getOldPrice() + "\n"
                                + "Дата: " + report.getLastDate() + "\n"
                                + "Последняя цена: " + report.getLastPrice();
                        msg = msg + messageAllSyncText + "\n\n";
                        telegramService.executeMessage(
                                SendMessage.builder()
                                        .text(msg)
                                        .chatId(message.getChatId().toString())
                                        .build()
                        );
                    }


                    break;

                case Commands.SYNCHRONIZE:
                    actionHistoryService.saveOrUpdate(Commands.SYNCHRONIZE, message.getChatId());
                    replyKeyboardMarkup = Commands.getSyncAllReplyKeyboardMarkup(name);
                    JSONObject numberOfReports = usersService.getNumberOfReport(message, name);
                    for (String key : numberOfReports.keySet()) {
                        String msgText = "Дилер: " + key + "\n"
                                + "У вас " + numberOfReports.get(key).toString() + " несинхронизированных продукта";
                        telegramService.executeMessage(
                                SendMessage.builder()
                                        .text(msgText)
                                        .chatId(message.getChatId().toString())
                                        .replyMarkup(replyKeyboardMarkup)
                                        .build()
                        );
                    }
                    break;
                default:
                    BotUserModel userModel1 = usersService.getUserByChtId(message);
                    if (userModel1.getStatus() == Status.ACTIVE) {
                        name = message.getText();
                        telegramService.executeMessage(
                                SendMessage.builder()
                                        .text("Dealer")
                                        .chatId(message.getChatId().toString())
                                        .replyMarkup(Commands.getMenuKeyboard(name))
                                        .build()
                        );
                    } else {
                        telegramService.executeMessage(
                                SendMessage.builder()
                                        .text("Неизвестная команда")
                                        .chatId(message.getChatId().toString())
                                        .build()
                        );
                    }


                    break;
            }
        } else {
            telegramService.executeMessage(
                    SendMessage.builder()
                            .chatId(message.getChatId().toString())
                            .replyMarkup(Commands.getShareContactKeyBoard())
                            .text("Пожалуйста, поделитесь своим контактом для регистрации")
                            .build()
            );
        }
    }

    public Boolean isNumber(String value) {
        try {
            Long.parseLong(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}

