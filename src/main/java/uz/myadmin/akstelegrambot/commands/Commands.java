package uz.myadmin.akstelegrambot.commands;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import uz.myadmin.akstelegrambot.model.DealerModel;

import java.util.ArrayList;
import java.util.List;

public final class Commands {
    public static final String START = "/start";
    public static final String MENU = "Меню";
    public static final String REMAINING_GOODS = "Остаток товари";
    public static final String CHECK_STATUS_USER = "Проверить статус";
    public static final String BACK = "назад";
    public static final String GET_ACTIONS_OF_TODAY = "Сегодняшний операции";
    public static final String SYNCHRONIZE = "Синхронизировать";
    public static final String GET_ALL_SYNC = "Я хочу получить все";
    public static final String ALL_TOGETHER = "Все вместе";
    public static final String LIST_DEALERS = "Список дилеров";
    public static final String MAIN_MENU = "Главное меню";


    public static ReplyKeyboardMarkup getShareContactKeyBoard() {
        ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();
        markup.setResizeKeyboard(true);
        markup.setOneTimeKeyboard(false);
        markup.setSelective(true);

        List<KeyboardRow> rows = new ArrayList<>();

        KeyboardRow row = new KeyboardRow();
        KeyboardButton buttonAskContact = new KeyboardButton();
        buttonAskContact.setRequestContact(true);
        buttonAskContact.setText("Share contact");
        row.add(buttonAskContact);
        rows.add(row);
        markup.setKeyboard(rows);
        return markup;
    }

    public static ReplyKeyboardMarkup getMenuKeyboard(String name) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();
        markup.setResizeKeyboard(true);
        markup.setOneTimeKeyboard(false);
        markup.setSelective(true);

        List<KeyboardRow> rows = new ArrayList<>();
        String sync = "Синхронизировать";
        String remainingGoods = "Остаток товари";
        String todaysOperation = "Сегодняшний операции";
        String back = "назад";
        String mainmenu = "Главное меню";
        if (!name.isEmpty()) {
            sync = sync.concat(" /").concat(name);
            remainingGoods = remainingGoods.concat(" /").concat(name);
            todaysOperation = todaysOperation.concat(" /").concat(name);
            back = back.concat(" /").concat(name);
        }
        KeyboardRow row = new KeyboardRow();
        KeyboardButton button = new KeyboardButton(sync);
        KeyboardButton button1 = new KeyboardButton(remainingGoods);
        KeyboardButton button2 = new KeyboardButton(todaysOperation);
        KeyboardButton mainMenuButton = new KeyboardButton(mainmenu);
        KeyboardButton backButton = new KeyboardButton(back);
        row.add(button);
        rows.add(row);
        row = new KeyboardRow();
        row.add(button1);
        rows.add(row);
        row = new KeyboardRow();
        row.add(button2);
        rows.add(row);
        row = new KeyboardRow();
        row.add(backButton);
        row.add(mainMenuButton);
        rows.add(row);
        markup.setKeyboard(rows);
        return markup;
//        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
//        List<List<InlineKeyboardButton>> keyboard = new ArrayList();
//        List<InlineKeyboardButton> row = new ArrayList<>();
//        InlineKeyboardButton button = new InlineKeyboardButton("Синхронизировать");
//        InlineKeyboardButton button1 = new InlineKeyboardButton("Остаток товари");
//        InlineKeyboardButton button2 = new InlineKeyboardButton("Сегодняшний операции");
//        button.setCallbackData(id+" Синхронизировать");
//        row.add(button);
//        keyboard.add(row);
//        row = new ArrayList<>();
//        button1.setCallbackData(id+" Остаток товари");
//        row.add(button1);
//        keyboard.add(row);
//        row = new ArrayList<>();
//        button2.setCallbackData(id+" Сегодняшний операции");
//        row.add(button2);
//        keyboard.add(row);
//        row = new ArrayList<>();
//        keyboard.add(row);
//        inlineKeyboardMarkup.setKeyboard(keyboard);
//        return inlineKeyboardMarkup;
    }

    public static ReplyKeyboardMarkup getCheckActiveUser() {

        ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();
        markup.setResizeKeyboard(true);
        markup.setOneTimeKeyboard(false);
        markup.setSelective(true);

        List<KeyboardRow> rows = new ArrayList<>();

        KeyboardRow row1 = new KeyboardRow();
        KeyboardButton checkActivationUser = new KeyboardButton("Проверить статус");
        row1.add(checkActivationUser);
        rows.add(row1);
        markup.setKeyboard(rows);
        return markup;
    }

    public static ReplyKeyboardMarkup getSyncAllReplyKeyboardMarkup(String name) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();
        markup.setResizeKeyboard(true);
        markup.setOneTimeKeyboard(false);
        markup.setSelective(true);

        List<KeyboardRow> rows = new ArrayList<>();
        String iWantAll = "Я хочу получить все";
        String back = "назад";
        String mainmenu = "Главное меню";
        if (!name.isEmpty()) {
            iWantAll = iWantAll.concat(" /").concat(name);
            back = back.concat(" /").concat(name);
        }
        KeyboardRow row = new KeyboardRow();
        KeyboardRow row1 = new KeyboardRow();
        KeyboardButton button = new KeyboardButton(iWantAll);
        KeyboardButton button1 = new KeyboardButton(back);
        KeyboardButton button2 = new KeyboardButton(mainmenu);
        row.add(button);
        row1.add(button1);
        row1.add(button2);
        rows.add(row);
        rows.add(row1);
        markup.setKeyboard(rows);
        return markup;

//        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
//        List<List<InlineKeyboardButton>> keyBoard = new ArrayList<>();
//        List<InlineKeyboardButton> row = new ArrayList<>();
//        InlineKeyboardButton button = new InlineKeyboardButton();
//        button.setText("Я хочу получить все");
//        button.setCallbackData(id + " Я хочу получить все");
//        row.add(button);
//        keyBoard.add(row);
//        markup.setKeyboard(keyBoard);
//        return markup;
    }

    public static ReplyKeyboardMarkup getSubMenuReplyKeyboard() {
        ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();
        markup.setSelective(true);
        markup.setResizeKeyboard(true);
        markup.setOneTimeKeyboard(true);

        List<KeyboardRow> keyboard = new ArrayList();

        KeyboardRow row = new KeyboardRow();

        KeyboardButton button = new KeyboardButton();
        KeyboardButton button1 = new KeyboardButton();

        button.setText("Все вместе");
        button1.setText("Список дилеров");
        row.add(button);
        row.add(button1);

        keyboard.add(row);

        markup.setKeyboard(keyboard);

        return markup;
    }

    public static ReplyKeyboardMarkup getListOfDealersMarkup(List<DealerModel> dealerModels) {
        ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();
        markup.setSelective(true);
        markup.setResizeKeyboard(true);
        markup.setOneTimeKeyboard(true);
        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardRow row = new KeyboardRow();
        for (DealerModel dealerModel : dealerModels) {
            KeyboardButton button = new KeyboardButton(dealerModel.getName());
            row.add(button);
            keyboard.add(row);
            row = new KeyboardRow();
        }
        String mainmenu = "Главное меню";
        String back = "назад";

        KeyboardButton backButton = new KeyboardButton(back);
        KeyboardButton mainMenuButton = new KeyboardButton(mainmenu);
        row.add(backButton);
        row.add(mainMenuButton);
        keyboard.add(row);

        markup.setKeyboard(keyboard);
        return markup;
    }
}
