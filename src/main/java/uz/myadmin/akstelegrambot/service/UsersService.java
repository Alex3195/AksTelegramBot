package uz.myadmin.akstelegrambot.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import reactor.core.publisher.Mono;
import uz.myadmin.akstelegrambot.commands.Commands;
import uz.myadmin.akstelegrambot.constant.Status;
import uz.myadmin.akstelegrambot.model.BotUserModel;
import uz.myadmin.akstelegrambot.model.DealerModel;
import uz.myadmin.akstelegrambot.model.TelegramSyncReport;

import java.util.*;

@Service
public class UsersService {
    private final WebClient webClient;
    private final TelegramService telegramService;

    public UsersService() {
//        String baseUrl = "http://localhost:8088/bot/api";
        String baseUrl = "http://localhost:9095/bot/api";
        webClient = WebClient.create(baseUrl);
        telegramService = new TelegramService();
    }

//    public Mono<Long> getChatId() {
//        return this.webClient.get().uri("/hello").accept(MediaType.APPLICATION_JSON)
//                .retrieve()
//                .bodyToMono(BotUserModel.class)
//                .map(BotUserModel::getChatId);
//    }

    public List<BotUserModel> getAll() {
        List<BotUserModel> res = new ArrayList<>();

        Object[] array = this.webClient.post().uri("/get-all-bot-users")
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_JSON)
                .retrieve().onStatus(HttpStatus::isError, clientResponse -> {
                    return Mono.error(new Exception("Error"));
                }).bodyToMono(Object[].class).block();
        for (Object data : array) {
            BotUserModel report = new BotUserModel();
            Map<String, Object> obj = (LinkedHashMap) data;
            report.setChatId(Long.parseLong(obj.get("chatId").toString()));
            report.setStatus(obj.get("status").toString().equalsIgnoreCase(Status.PASSIVE.toString()) ? Status.PASSIVE : Status.ACTIVE);
            if (obj.get("type") != null) {
                report.setType(obj.get("type").toString());
            }
            res.add(report);
        }
        return res;
    }

    public BotUserModel saveUser(Message message) {

        BotUserModel model = new BotUserModel();
        model.setChatId(message.getChatId());
        model.setUserName(message.getFrom().getUserName());
        model.setFirstName(message.getFrom().getFirstName());
        model.setLastName(message.getFrom().getLastName());
        model.setContact(message.getContact().getPhoneNumber());
        model.setStatus(Status.PASSIVE);

        Object user = this.webClient.put().uri("/add")
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(model)
                .retrieve().onStatus(HttpStatus::isError, clientResponse -> {
                    return Mono.error(new Exception("error"));
                })
                .bodyToMono(Object.class).block();
        Map<String, Object> map = (LinkedHashMap) user;
        BotUserModel userModel = new BotUserModel();
        userModel.setId(Long.valueOf(map.get("id").toString()));
        userModel.setChatId(Long.parseLong(map.get("chatId").toString()));
        userModel.setUserName(map.get("userName").toString());
        userModel.setFirstName(map.get("firstName").toString());
        if (map.containsKey("lastName")) {

            userModel.setLastName(map.get("lastName").toString());
        }
        userModel.setStatus(Status.ACTIVE.name().equalsIgnoreCase(map.get("status").toString()) ? Status.ACTIVE : Status.PASSIVE);
        List<Long> dealerIds = new ArrayList<>();
        List<DealerModel> dealers = new ArrayList<>();
        if (map.get("dealerId") != null) {
            for (Object id : (ArrayList) map.get("dealerId")) {
                dealerIds.add(Long.valueOf(id.toString()));
            }
            userModel.setDealerId(dealerIds);

            for (LinkedHashMap linkedMap : (ArrayList<LinkedHashMap>) map.get("dealer")) {
                DealerModel dealerModel = new DealerModel();
                dealerModel.setId(Long.parseLong(linkedMap.get("id").toString()));
                dealerModel.setName(linkedMap.get("name").toString());
                dealers.add(dealerModel);
            }
            userModel.setDealerModels(dealers);
        }

        return userModel;
    }

    public BotUserModel checkUserStatus(Message message) {

        BotUserModel model = new BotUserModel();
        model.setChatId(message.getChatId());
        model.setUserName(message.getFrom().getUserName());
        model.setFirstName(message.getFrom().getFirstName());
        model.setLastName(message.getFrom().getLastName());
//        model.setContact(message.getContact().getPhoneNumber());
//        model.setStatus(Status.PASSIVE);

        return this.webClient.get().uri("/getByChatId/" + message.getChatId())
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
//                .contentType(MediaType.APPLICATION_JSON)
//                .bodyValue(model)
                .retrieve().onStatus(HttpStatus::isError, clientResponse -> {
                    return Mono.error(new Exception("error"));
                })
                .bodyToMono(BotUserModel.class).block();
    }

    public JSONObject getNumberOfReport(Message message, String name) {
        BotUserModel model = new BotUserModel();
        model.setChatId(message.getChatId());
        model.setDealerName(name);
        Object map = this.webClient.post().uri("/get-number-of-reports-not-sync")
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(model)
                .retrieve().onStatus(HttpStatus::isError, clientResponse -> {
                    return Mono.error(new Exception("error"));
                })
                .bodyToMono(Object.class).block();
        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap = (LinkedHashMap) map;
        JSONObject data = new JSONObject();
        for (String key : jsonMap.keySet()) {
            data.put(key, jsonMap.get(key));
        }
        return data;

    }

    public List<TelegramSyncReport> getAllReportByDealerId(Message message, String name) {
        List<TelegramSyncReport> res = new ArrayList<>();
        BotUserModel model = new BotUserModel();
        model.setChatId(message.getChatId());
        model.setDealerName(name);
        Object[] array = this.webClient.post().uri("/get-all-reports-by-dealer")
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(model)
                .retrieve().onStatus(HttpStatus::isError, clientResponse -> {
                    return Mono.error(new Exception("Error"));
                }).bodyToMono(Object[].class).block();
        for (Object data : array) {
            Map<String, List<Object>> objMap = (LinkedHashMap) data;
            for (String key : objMap.keySet()) {
                for (Object obj : objMap.get(key)) {
                    TelegramSyncReport report = new TelegramSyncReport();
                    Map<String, Object> map = (LinkedHashMap) obj;
                    if (map.get("productName") == null)
                        continue;
                    report.setProductName(map.get("productName") != null ? map.get("productName").toString() : "-");
                    if (map.get("oldDate") != null) {
                        report.setOldDate(map.get("oldDate").toString());
                    }
                    report.setOldPrice(map.get("oldPrice").toString());
                    report.setLastDate(map.get("lastDate").toString());
                    report.setLastPrice(map.get("lastPrice").toString());
                    report.setDate(map.get("date").toString());
                    res.add(report);
                }
            }

        }
        return res;
    }

    public Map getReportRemainingGoods(Message message, String name) {
        List<TelegramSyncReport> res = new ArrayList<>();
        BotUserModel model = new BotUserModel();
        model.setChatId(message.getChatId());
        model.setDealerName(name);
        Object data = this.webClient.post().uri("/get-report-amount-of-products")
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(model)
                .retrieve().onStatus(HttpStatus::isError, clientResponse -> {
                    return Mono.error(new Exception("Error"));
                }).bodyToMono(Object.class).block();
        Map jsonObject = (Map) data;
        return jsonObject;
    }

    public Map getActionsOfToday(Message message, String name) {
        List<TelegramSyncReport> res = new ArrayList<>();
        BotUserModel model = new BotUserModel();
        model.setChatId(message.getChatId());
        model.setDealerName(name);
        Object data = this.webClient.post().uri("/get-actions-of-today")
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(model)
                .retrieve().onStatus(HttpStatus::isError, clientResponse -> {
                    return Mono.error(new Exception("Error"));
                }).bodyToMono(Object.class).block();
        Map jsonObject = (Map) data;
        return jsonObject;
    }

    public BotUserModel getUserStatus(Message message) {
        String url = "/getByChatId/".concat(String.valueOf(message.getChatId()));
        Object model = this.webClient.get().uri(url)
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .retrieve().onStatus(HttpStatus::isError, clientResponse -> {
                    return Mono.error(new Exception("error"));
                })
                .bodyToMono(Object.class).block();

        Map<String, Object> map = (LinkedHashMap) model;
        BotUserModel userModel = new BotUserModel();
        userModel.setId(Long.valueOf(map.get("id").toString()));
        userModel.setChatId(Long.parseLong(map.get("chatId").toString()));
        userModel.setUserName(map.get("userName").toString());
        userModel.setFirstName(map.get("firstName").toString());
        userModel.setLastName(map.get("lastName")!=null?map.get("lastName").toString():"");
        userModel.setStatus(Status.ACTIVE.name().equalsIgnoreCase(map.get("status").toString()) ? Status.ACTIVE : Status.PASSIVE);
        List<Long> dealerIds = new ArrayList<>();
        List<DealerModel> dealers = new ArrayList<>();
        for (Object id : (ArrayList) map.get("dealerId")) {
            dealerIds.add(Long.valueOf(id.toString()));
        }
        userModel.setDealerId(dealerIds);
        for (Object obj : (ArrayList<DealerModel>) map.get("dealer")) {
            Map<String, Object> mapDealer = (LinkedHashMap) obj;

            DealerModel dealerModel = new DealerModel();
            dealerModel.setId(Long.valueOf(mapDealer.get("id").toString()));
            dealerModel.setName(mapDealer.get("name").toString());
            dealers.add(dealerModel);
        }
        userModel.setDealerModels(dealers);
        return userModel;
    }

    public BotUserModel getUserByChtId(Message message) {
        String url = "/getByChatId/".concat(String.valueOf(message.getChatId()));

        Object model = this.webClient.get().uri(url)
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .retrieve().onStatus(HttpStatus::isError, clientResponse -> {
                    return Mono.error(new Exception("error"));
                })
                .bodyToMono(Object.class).block();
        if(model==null){
            return null;
        }
        Map<String, Object> map = (LinkedHashMap) model;
        BotUserModel userModel = new BotUserModel();
        userModel.setId(Long.valueOf(map.get("id").toString()));
        userModel.setChatId(Long.parseLong(map.get("chatId").toString()));
        userModel.setUserName(map.get("userName").toString());
        userModel.setFirstName(map.get("firstName").toString());
        userModel.setLastName(map.get("lastName")!=null?map.get("lastName").toString():"");
        userModel.setStatus(Status.ACTIVE.name().equalsIgnoreCase(map.get("status").toString()) ? Status.ACTIVE : Status.PASSIVE);
        List<Long> dealerIds = new ArrayList<>();
        List<DealerModel> dealers = new ArrayList<>();
        for (Object id : (ArrayList) map.get("dealerId")) {
            dealerIds.add(Long.valueOf(id.toString()));
        }
        userModel.setDealerId(dealerIds);
        for (Object obj : (ArrayList<DealerModel>) map.get("dealer")) {
            Map<String, Object> mapDealer = (LinkedHashMap) obj;

            DealerModel dealerModel = new DealerModel();
            dealerModel.setId(Long.valueOf(mapDealer.get("id").toString()));
            dealerModel.setName(mapDealer.get("name").toString());
            dealers.add(dealerModel);
        }
        userModel.setType(map.get("typeName").toString());
        userModel.setDealerModels(dealers);
        return userModel;
    }

    public Map getReportRemainingGoodsAllTogether(Message message, String name) {
        List<TelegramSyncReport> res = new ArrayList<>();
        BotUserModel model = new BotUserModel();
        model.setChatId(message.getChatId());
        model.setDealerName(name);
        Object data = this.webClient.post().uri("/get-report-amount-of-products-all")
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(model)
                .retrieve().onStatus(HttpStatus::isError, clientResponse -> {
                    return Mono.error(new Exception("Error"));
                }).bodyToMono(Object.class).block();
        Map jsonObject = (Map) data;
        return jsonObject;
    }


}
