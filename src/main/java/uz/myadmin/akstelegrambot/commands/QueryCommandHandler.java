package uz.myadmin.akstelegrambot.commands;

import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.myadmin.akstelegrambot.model.BotUserModel;
import uz.myadmin.akstelegrambot.model.TelegramSyncReport;
import uz.myadmin.akstelegrambot.service.ActionHistoryService;
import uz.myadmin.akstelegrambot.service.TelegramService;
import uz.myadmin.akstelegrambot.service.UsersService;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class QueryCommandHandler implements CommandHandler<CallbackQuery> {
    private final TelegramService telegramService;
    private final ActionHistoryService actionHistoryService;
    private ReplyKeyboardMarkup replyKeyboardMarkup;
    private final UsersService usersService;

    @Override
    public void executeCommand(CallbackQuery query, String data) throws TelegramApiException {
        actionHistoryService.saveOrUpdate(data, query.getMessage().getChatId());
        Long id = 0L;
        String commad = "";
        try {
            id = Long.valueOf(data.split(" ")[0].toString());
            commad = data.split(" ")[0];
            commad = data.substring(commad.length() + 1, data.length());
        } catch (NumberFormatException e) {
            commad = data;
            e.printStackTrace();
        }
        switch (commad) {
            case Commands.ALL_TOGETHER:
                actionHistoryService.saveOrUpdate(Commands.ALL_TOGETHER, query.getMessage().getChatId());
                replyKeyboardMarkup = Commands.getMenuKeyboard("");
                telegramService.executeMessage(
                        SendMessage.builder()
                                .text(Commands.MENU)
                                .replyMarkup(replyKeyboardMarkup)
                                .chatId(query.getMessage().getChatId().toString())
                                .build()
                );
                break;
            case Commands.SYNCHRONIZE:
                actionHistoryService.saveOrUpdate(Commands.SYNCHRONIZE, query.getMessage().getChatId());
                replyKeyboardMarkup = Commands.getSyncAllReplyKeyboardMarkup("");
                JSONObject numberOfReports = usersService.getNumberOfReport(query.getMessage(), "");
                for (String key : numberOfReports.keySet()) {
                    telegramService.executeMessage(
                            SendMessage.builder()
                                    .text("У вас " + numberOfReports.get(key).toString() + " несинхронизированных продукта")
                                    .chatId(query.getMessage().getChatId().toString())
                                    .replyMarkup(replyKeyboardMarkup)
                                    .build()
                    );
                }
                break;
            case Commands.GET_ALL_SYNC:
                actionHistoryService.saveOrUpdate(Commands.GET_ALL_SYNC, query.getMessage().getChatId());
                replyKeyboardMarkup = Commands.getMenuKeyboard("");
                List<TelegramSyncReport> reportList = usersService.getAllReportByDealerId(query.getMessage(), "");
                String msg = "------  Товары на которые изменилась цена  ------\n\n";

                for (TelegramSyncReport report : reportList) {
                    String messageAllSyncText = "Название продукта: " + report.getProductName() + "\n"
                            +"Время записи данных в базу данных: "+report.getDate()+"\n"
                            + "Дата: " + report.getOldDate() + "\n"
                            + "Старая цена: " + report.getOldPrice() + "\n"
                            + "Дата: " + report.getLastDate() + "\n"
                            + "Последняя цена: " + report.getLastPrice();
                    msg = msg + messageAllSyncText + "\n\n";
                    telegramService.executeMessage(
                            SendMessage.builder()
                                    .text(msg)
                                    .chatId(query.getMessage().getChatId().toString())
                                    .build()
                    );
                }


                break;
            case Commands.REMAINING_GOODS:
                actionHistoryService.saveOrUpdate(Commands.REMAINING_GOODS, query.getMessage().getChatId());
                BotUserModel userModel = usersService.getUserByChtId(query.getMessage());
                if (userModel.getType().equalsIgnoreCase("OFFICE_MANAGER")) {
                    Map dataMap = usersService.getReportRemainingGoodsAllTogether(query.getMessage(), "");
                    String messageTextRemainingGoods = "------  Остаток товари  ------\n";
                    for (Object key : dataMap.keySet()) {
                        Map innerData = (LinkedHashMap) dataMap.get(key);
                        messageTextRemainingGoods = messageTextRemainingGoods.concat(
                                "Дата: " + innerData.get("date").toString() + " \n"
                                        + "Дилер: " + innerData.get("dealerName").toString() + " \n"
                                        + "Количество видов товаров: " + innerData.get("countProductTypes").toString() + "\n"
                                        + "Сумма товара : " + innerData.get("countProducts") + "\n"
                                        + "Сумма стоимости товара: " + innerData.get("sumProductPrice") + "\n\n"
                        );
                    }
                    telegramService.executeMessage(
                            SendMessage.builder()
                                    .text(messageTextRemainingGoods)
                                    .chatId(query.getMessage().getChatId().toString())
                                    .build());
                } else {
                    Map dataMap = usersService.getReportRemainingGoods(query.getMessage(), "");

                    String messageTextRemainingGoods = "------  Остаток товари  ------\n";
                    for (Object key : dataMap.keySet()) {
                        Map innerData = (LinkedHashMap) dataMap.get(key);
                        messageTextRemainingGoods = messageTextRemainingGoods.concat(
                                "Дата: " + innerData.get("date").toString() + " \n"
                                        + "Дилер: " + innerData.get("dealerName").toString() + " \n"
                                        + "Количество видов товаров: " + innerData.get("countProductTypes").toString() + "\n"
                                        + "Сумма товара : " + innerData.get("countProducts") + "\n"
                                        + "Сумма стоимости товара: " + innerData.get("sumProductPrice") + "\n\n"
                        );
                    }
                    telegramService.executeMessage(
                            SendMessage.builder()
                                    .text(messageTextRemainingGoods)
                                    .chatId(query.getMessage().getChatId().toString())
                                    .build());
                }

                break;
            case Commands.GET_ACTIONS_OF_TODAY:
                actionHistoryService.saveOrUpdate(Commands.GET_ACTIONS_OF_TODAY, query.getMessage().getChatId());
                Map dataActions = usersService.getActionsOfToday(query.getMessage(), "");
                HashMap<String, Object> dataSelling = (HashMap<String, Object>) dataActions.get("selling");
                HashMap<String, Object> dataComing = (HashMap<String, Object>) dataActions.get("coming");
                HashMap<String, Object> dataReturn = (HashMap<String, Object>) dataActions.get("return");
                HashMap<String, Map<String, Object>> dataShifting = (HashMap<String, Map<String, Object>>) dataActions.get("shifting");
                HashMap<String, Object> dataBase = (HashMap<String, Object>) dataReturn.get("base");
                HashMap<String, Object> dataClient = (HashMap<String, Object>) dataReturn.get("client");
                String messageTextActionOfToday = "------  Сегодняшний операции  ------\n" +
                        "Date: " + dataActions.get("date").toString() + "\n"
                        + "Продажи: \n" + " \t Количество операций: " + dataSelling.get("invoiceQty").toString() + "\n"
                        + " \t Количество продуктов: " + dataSelling.get("productQty").toString() + "\n"
                        + " \t Сумма: " + dataSelling.get("totalSum").toString() + " " + dataSelling.get("currency").toString() + "\n\n"
                        + "Прыход: \n \t Количество продукти : " + dataComing.get("comingQty").toString() + "\n"
                        + "\t \t Сумма: " + dataComing.get("comingSum").toString() + " " + dataComing.get("currency") + "\n\n"
                        + "Возврадь : \n \t Возврадь база : \n \t \t \t Количество возврадь : " + dataBase.get("returnBaseQty").toString() + "\n"
                        + " \t \t \t Сумма возврадь :" + dataBase.get("returnBaseSum").toString() + " " + dataBase.get("currency").toString() + "\n"
                        + "\t Возврадь клиент: \n \t \t \t Количество возврадь : " + dataClient.get("returnClientQty").toString() + "\n"
                        + " \t \t \t Сумма возврадь :" + dataClient.get("returnClientSum").toString() + " " + dataClient.get("currency").toString() + "\n\n"
                        + " Перемешение :\n \t Количество товаров, переданных дилеру:" + dataShifting.get("transfer").get("shiftingQty").toString() + " \n"
                        + " \t Сумма: " + dataShifting.get("transfer").get("shiftingSum").toString() + " " + dataShifting.get("transfer").get("currency").toString()
                        + "\n \t Количество продуктов, полученных с сервера:" + dataShifting.get("transfer").get("shiftingQty").toString() + " \n"
                        + " \t Сумма: " + dataShifting.get("transfer").get("shiftingSum").toString() + " " + dataShifting.get("transfer").get("currency").toString();

                telegramService.executeMessage(
                        SendMessage.builder()
                                .text(messageTextActionOfToday)
                                .chatId(query.getMessage().getChatId().toString())
                                .build()
                );
                break;
            case Commands.BACK:
                String action = actionHistoryService.getLastActionByChatId(query.getMessage().getChatId());
                switch (action) {
                    case Commands.REMAINING_GOODS:
                        replyKeyboardMarkup = Commands.getMenuKeyboard("");
                        break;
                    case Commands.ALL_TOGETHER:
                        replyKeyboardMarkup = Commands.getMenuKeyboard("");
                        break;
                    case Commands.GET_ALL_SYNC:
                        replyKeyboardMarkup = Commands.getMenuKeyboard("");
                    case Commands.SYNCHRONIZE:
                        replyKeyboardMarkup = Commands.getSyncAllReplyKeyboardMarkup("");
                        break;
                    default:
                        replyKeyboardMarkup = Commands.getMenuKeyboard("");
                        break;
                }
                telegramService.executeMessage(
                        SendMessage.builder()
                                .chatId(query.getMessage().getChatId().toString())
                                .replyMarkup(replyKeyboardMarkup)
                                .build()
                );
                break;
            default:
                telegramService.executeMessage(
                        SendMessage.builder()
                                .chatId(query.getMessage().getChatId().toString())
                                .replyMarkup(Commands.getMenuKeyboard(""))
                                .text(commad)
                                .build()
                );
        }
    }
}
