package uz.myadmin.akstelegrambot.handler.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.myadmin.akstelegrambot.commands.QueryCommandHandler;
import uz.myadmin.akstelegrambot.handler.Handler;
import uz.myadmin.akstelegrambot.model.BotUserModel;
import uz.myadmin.akstelegrambot.service.TelegramService;
import uz.myadmin.akstelegrambot.service.UsersService;

@Service
@RequiredArgsConstructor
public class CallBackQueryHandler implements Handler<CallbackQuery> {
    private final TelegramService telegramService;
    private final UsersService usersService;
    private final QueryCommandHandler queryCommandHandler;

    @Override
    public void handleMessage(CallbackQuery query) throws TelegramApiException {
        BotUserModel userModel = usersService.getUserByChtId(query.getMessage());
       String dat =  query.getData();
       queryCommandHandler.executeCommand(query,dat);
    }
}
